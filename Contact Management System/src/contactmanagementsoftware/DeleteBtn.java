/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package contactmanagementsoftware;

/**
 *
 * @author ACER
 */
public class DeleteBtn extends Observer{
    private boolean actionPermitted = false;
    
    public DeleteBtn(Subject subject){
        this.subject = subject;
        this.subject.attach(this);
    }

    @Override
    public void update() {
        System.out.println("DeleteBtn_updated");
        if(subject.getState() == "delete"){
            actionPermitted = true;
        }
    }
    
    public boolean useAction(){
        if(actionPermitted){
            System.out.println("actionPermitted_delete");
            return true;
        }else{
            return false;
        }
    }
}
