/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package contactmanagementsoftware;

/**
 *
 * @author ACER
 */
public class SaveAsFileBtn extends Observer {
    private boolean actionPermitted = false;
    
    public SaveAsFileBtn(Subject subject){
        this.subject = subject;
        this.subject.attach(this);
    }

    @Override
    public void update() {
        System.out.println("SAFBtn_updated");
        if(subject.getState() == "SAF"){
            actionPermitted = true;
        }
    }
    
    public boolean useAction(){
        if(actionPermitted){
            System.out.println("actionPermitted_SAF");
            return true;
        }else{
            return false;
        }
    }
}
