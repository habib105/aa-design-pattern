package contactmanagementsoftware;

public class AcquaintancesFactory {

    /**
     * Factory Design
     *
     * @param type
     * @return
     */
    public Acquaintances factory(int type) {
        switch (type) {
            case 0:
                return new PersonalFriends();
            case 1:
                return new Relatives();
            case 2:
                return new ProfessionalFriends();
            case 3:
                return new CasualAcquaintances();
        }
        return null;
    }
}
