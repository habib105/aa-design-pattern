/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package contactmanagementsoftware;
import contactmanagementsoftware.Acquaintances;
import contactmanagementsoftware.MUI;
import contactmanagementsoftware.Relatives;

import java.io.Serializable;

/**
 *
 * @author User
 */
public class RelativesStrategy implements Strategy, Serializable {

    @Override
    public void setText(Acquaintances acquaintances) {
        MUI mui = MUI.getInstance();
        Relatives  rel = (Relatives)acquaintances;
        mui.one.setText(rel.getBDate());
        mui.two.setText(rel.getLDate());
        
    }
    
}
