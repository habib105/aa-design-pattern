/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package contactmanagementsoftware;
import contactmanagementsoftware.Acquaintances;
import contactmanagementsoftware.MUI;
import contactmanagementsoftware.ProfessionalFriends;

import java.io.Serializable;

/**
 *
 * @author User
 */
public class ProfessionalFriendsStrategy  implements Strategy, Serializable {

    @Override
    public void setText(Acquaintances acquaintances) {
        MUI mui = MUI.getInstance();
        ProfessionalFriends proF = (ProfessionalFriends)acquaintances;
        mui.one.setText(proF.getCommonInterests());
    }
    
    
}
