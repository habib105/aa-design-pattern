/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package contactmanagementsoftware;
import contactmanagementsoftware.Acquaintances;
import contactmanagementsoftware.MUI;
import contactmanagementsoftware.CasualAcquaintances;

import java.io.Serializable;

/**
 *
 * @author User
 */
public class CasualAcquaintancesStrategy implements Strategy, Serializable {

    @Override
    public void setText(Acquaintances acquaintances) {
        MUI mui = MUI.getInstance();
        CasualAcquaintances ca = (CasualAcquaintances)acquaintances;
        mui.one.setText(ca.getWhenWhere());
        mui.two.setVisible(true);
        mui.three.setVisible(true);
        mui.two.setText(ca.getCircumstances());
        mui.three.setText(ca.getOtherInfo());
    }
    
}
