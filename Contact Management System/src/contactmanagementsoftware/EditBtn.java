/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package contactmanagementsoftware;

/**
 *
 * @author ACER
 */
public class EditBtn extends Observer {
    private boolean actionPermitted = false;
    
    public EditBtn(Subject subject){
        this.subject = subject;
        this.subject.attach(this);
    }

    @Override
    public void update() {
        System.out.println("EditBtn_updated");
        if(subject.getState() == "update"){
            actionPermitted = true;
        }
    }
    
    public boolean useAction(){
        if(actionPermitted){
            System.out.println("actionPermitted_update");
            return true;
        }else{
            return false;
        }
    }
}
