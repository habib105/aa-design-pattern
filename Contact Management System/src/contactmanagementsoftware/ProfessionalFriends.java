package contactmanagementsoftware;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;
import java.util.Scanner;
import java.util.stream.Collector;

public class ProfessionalFriends extends Acquaintances implements Serializable{

    private String CommonInterests;
    public static int numberProF = 0;

    ProfessionalFriends(){
        this.setStrategy(new ProfessionalFriendsStrategy());
        numberProF++;
    }

    @Override
    public void setText(Acquaintances acquaintances) {
        acquaintances.setText(acquaintances);
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void setName(String Name) {
        super.setName(Name);
    }

    @Override
    public String getMobileNo() {
        return super.getMobileNo();
    }

    @Override
    public void setMobileNo(String MobileNo) {
        super.setMobileNo(MobileNo);
    }

    @Override
    public String getEmail() {
        return super.getEmail();
    }

    @Override
    public void setEmail(String Email) {
        super.setEmail(Email);
    }



    public String getCommonInterests() {
        return CommonInterests;
    }

    public void setCommonInterests(String CommonInterests) {
        Scanner reader = new Scanner(System.in);
        if(!CommonInterests.isEmpty())
            this.CommonInterests = CommonInterests;
        else{
            System.out.println("Enter at least one character");
            setCommonInterests(reader.nextLine());
        }
    }
    public  void setFirstInfo(String Info){
        setCommonInterests(Info);
    };



    public  void setSecondInfo(String Info1 ){
    };



    public  void setThirdInfo (String Info2 ){
    };

    public int checkInfo(){

        return 1;}
}


