/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package contactmanagementsoftware;

/**
 *
 * @author ACER
 */
public class ViewFullDetailBtn extends Observer{
    private boolean actionPermitted = false;
    
    public ViewFullDetailBtn(Subject subject){
        this.subject = subject;
        this.subject.attach(this);
    }

    @Override
    public void update() {
        System.out.println("VFDBtn_updated");
        if(subject.getState() == "VFD"){
            actionPermitted = true;
        }
    }
    
    public boolean useAction(){
        if(actionPermitted){
            System.out.println("actionPermitted_VFD");
            return true;
        }else{
            return false;
        }
    }
}
